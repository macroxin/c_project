# c_project

#### 介绍
本人开发的c程序

#### 开发环境

 1.Linux 
 2.GCC

#### 使用说明

-- Linux下 添加getch.h 到库。
1. GCC编译的相关知识
2. makefile的使用

#### 我的博客

[传送门](http://blog.csdn.net/Ikaros_521)
链接：https://blog.csdn.net/Ikaros_521

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)